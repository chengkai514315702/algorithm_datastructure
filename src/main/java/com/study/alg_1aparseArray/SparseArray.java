package com.study.alg_1aparseArray;


/**
 * 稀疏数组
 *
 */
public class SparseArray {

    public static void main(String[] args) {
        //创建一个原始的二维数组 11* 11

        //0-表示没有棋子，1-表示黑子，2-表示蓝子
        //创建一个棋盘
        int chessArray [] []= new int[11][11];
        //存放棋子
        chessArray[1][2] = 1; //第二行第三列
        chessArray[2][3] = 2;    //第三行第四列
        //输出二维数组
        for(int [] row: chessArray){
            for(int data: row){
                System.out.printf("%d \t" ,data);
            }
            System.out.println();
        }
        //将二维数据转化为稀疏数组,第一步获取稀疏数组有效数据个数
        int sum = 0;
        for(int i=0 ;i<11;i++){
               for (int j = 0 ;j< 11 ;j++){
                   if(chessArray[i][j] != 0){
                       sum++;
                   }
               }
        }

        //创建稀疏数组
        int sparseArray [] [] = new int[sum+1][3];

        //初始化第一行数据
        int count =0;
        sparseArray[0][0] = 11;
        sparseArray[0][1] = 11;
        sparseArray[0][2] = sum;
        for(int i=0 ;i<11;i++){
            for (int j = 0 ;j< 11 ;j++){
                if(chessArray[i][j] != 0){
                    //此处写行数
                    count++;
                    sparseArray[count][0] = i;
                    sparseArray[count][1] = j;
                    sparseArray[count][2] = chessArray[i][j] ;
                }
            }
        }
        //输出稀疏数组
        System.out.println();
        for(int i=0 ;i<sparseArray.length;i++){
            System.out.printf("%d\t %d\t %d\t\n" ,sparseArray[i][0],sparseArray[i][1],sparseArray[i][2]);
        }
        System.out.println();
        //将稀疏数组转化为二维数组
        //获取稀疏数组大小
        int chessArr[][] = new int[sparseArray[0][0]][sparseArray[0][1]];

        //创建二维数组
        for(int i = 1;i < sparseArray.length;i++){
            //第几行，第几列，数据
            chessArr[sparseArray[i][0]][sparseArray[i][1]] = sparseArray[i][2];
        }

        //遍历二维数组
        for(int [] row: chessArr){
            for (int data: row){
                System.out.printf("%d \t" ,data);
            }
            System.out.println();
        }
    }
}
